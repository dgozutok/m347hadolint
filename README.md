# M347HadoLint

## Resultat
- C:\Path DL3060 **info**: `yarn cache clean` missing after `yarn install` was run.
- C:\Path DL4003 **warning**: Multiple `CMD` instructions found. If you list more than one `CMD` then only the last `CMD` will take effect

<p style="font-family: 'Courier New', Courier, monospace;">This text is in Courier New font.</p>

### Nicht-optimiertes Dockerfile

```
FROM node:18-alpine
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
CMD [ "", "executable" ]
EXPOSE 3000
```



### Optimiertes Dockerfile

```
FROM node:18-alpine
WORKDIR /app
COPY . .
RUN yarn install --production && yarn cache clean
CMD ["node", "src/index.js"]
EXPOSE 3000
```
